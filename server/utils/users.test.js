const expect = require('expect');

const {Users} = require('./users');

describe('Users', () => {

  beforeEach(() => {
    users = new Users();
    users.users = [
      {
        id: '1',
        name: 'Mike',
        room: 'Node Course'
      },
      {
        id: '2',
        name: 'Jen',
        room: 'Go Course'
      },
      {
        id: '3',
        name: 'Bob',
        room: 'Python Course'
      }
    ]
  });
  it('should add new user', () => {
    const users = new Users();
    const user = {
      id: '123',
      name: 'ossi',
      room: 'kingdon'
    };

    const resUser = users.addUser(user.id, user.name, user.room);

    expect(users.users).toEqual([user]);
  });

  it('should remove a user', () => {
    const userId = '1';
    const user = users.removeUser(userId);

    expect(user.id).toBe(userId);
    expect(users.users.length).toBe(2);
  });

  it('should not remove a user', () => {
    var userId = '456';
    const user = users.getUser(userId);

    expect(user).toNotExist();
    expect(users.users.length).toBe(3);
  });

  it('should find user', () => {
    const userId = '2';
    const user = users.getUser(userId);

    expect(user.id).toBe(userId);
  });

  it('should return names for node course', () => {
    const userList = users.getUserList('Node Course');

    expect(userList).toEqual(['Mike']);
  });

  it('should return names for Go course', () => {
    const userList = users.getUserList('Node Course');

    expect(userList).toEqual(['Jen']);
  });
})
