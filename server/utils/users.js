[{
  id: ''
}]

//addUser(id, name, room)

//removeUser(id);

//getUser(id)

// getUserList(room)


class Users {
  constructor () {
    this.users = [];
  }

  addUser(id, name, room) {
    const user = {id, name, room};
    this.users.push(user);
    return user;
  }

  removeUser(id) {
    const user = this.getUser(id);

    if (user) {
      this.users = this.users.filter((user) => user.id !== id);
    }

    return user;
  }

  getUser (id) {
    return this.users.filter((user) => user.id === id)[0];
  }

  /**
  * Returns list of users in room as an array
  *
  */
  getUserList(room) {
    const users = this.users.filter((user) => user.room === room);
    const namesArray = users.map((user) => user.name);

    return namesArray;
  }
}

module.exports = {Users};

// class Person {
//   constructor (name, age) {
//     this.name = name;
//     this.age = age;
//   }
//   getUserDescription () {
//     return `${this.name} is 1 year(s) old`;
//   }
// }
//
// const me = new Person ('Veijo', 698);
// const description = me.getUserDescription();
// console.log(description);
// console.log('this.name', me.name);
// console.log('this.age', me.age);

//
// const users = [];
//
// const addUser = (id, name, room) => {
//   users.psuh({})
// }
