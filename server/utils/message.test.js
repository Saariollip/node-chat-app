const expect = require('expect')

const {generateMessage, generateLocationMessage} = require('./message')

describe('generateMessage', () => {
  it('should generate correct message object', () => {
    const from = 'ossi';
    const text = 'viesti avaruudesta';
    const msg = generateMessage(from, text);
    console.log(msg);

    expect(msg.createdAt).toBeA('number');
    expect(msg).toInclude({from, text});
  });
});

describe('generateLocationMessage', () => {
  it('Should generate correct location object', () => {
    const from = 'Deb';
    var latitude = 15;
    var longitude = 19;
    var url = 'https://www.google.com/maps?q=15,19';
    var message = generateLocationMessage(from, latitude, longitude);

    expect(message.createdAt).toBeA('number');
    expect(message).toInclude({from, url});
  });
});
