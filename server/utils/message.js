const moment = require('moment');

const generateMessage = (from, text) => {
  console.log('input', text);
  // TODO: change to switch statement
  if (text === 'sam' || text === 'SAM' || text === 'Sam' || text === 'sam.') {
    from = 'Sam Dog';
    text = `Wuff, wuff! Sam is here to provide you the following services:
    <br>search - search google
    <br>play - Play video from youtube. You can search videos typing eg. play batman
    <br>flip - flip a coin
    <br>location - Share your location.
    <br>forecast - Check forecast in your location. If GPS is disabled type eg. forecast Oulu`;
  }
  else if (text === 'search' || text === 'Search') {
    from = 'Sam Dog'
    text = 'Google Search is currently under development.'
  }
  else if (text === 'play' || text === 'Play') {
    from = 'Sam Dog'
    text = 'Playing video.'
  }
  else if (text === 'flip' || text === 'Flip' ) {
    from = 'Sam Dog';
    text = 'Sam dog is flipping a bon... coin...<br>';
    text +=  Math.floor(Math.random() * 2) === 0 ? 'You got Heads, lucky you!' : 'You got Tails, my bad!';
  }
  else if (text === 'forecast' || text === 'Forecast') {
    from = 'Sam Dog'
    text = 'Forecast is currently under development.'
  }
  return {
    from,
    text,
    createdAt: moment().valueOf()
  };
};

const generateLocationMessage = (from, latitude, longitude) => {
  return {
    from,
    url: `https://www.google.com/maps?q=${latitude},${longitude}`,
    createdAt: moment().valueOf()
  };
};
module.exports = {generateMessage, generateLocationMessage};
