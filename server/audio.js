
const YoutubePlayer = require('youtube-player');

let player;
player = YoutubePlayer('video-player');

// 'loadVideoById' is queued until the player is ready to receive API calls.
player.loadVideoById('M7lc1UVf-VE');

// 'playVideo' is queue until the player is ready to received API calls and after 'loadVideoById' has been called.
player.playVideo();

// 'stopVideo' is queued after 'playVideo'.
player
    .stopVideo()
    .then(() => {
        // Every function returns a promise that is resolved after the target function has been executed.
    });
