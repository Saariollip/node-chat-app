const path = require('path');
const http = require('http');
const express = require('express');
const socketIO = require('socket.io');

const {generateMessage, generateLocationMessage} = require('./utils/message');
const {isRealString} = require('./utils/validation');
const {Users} = require('./utils/users');

const publicPath = path.join(__dirname, '../public');
const port = process.env.PORT || 3000;
const app = express();
const server = http.createServer(app);
const io = socketIO(server);
const users = new Users();

app.use(express.static(publicPath));

io.on('connection', (socket) => {
  console.log('New user connected.');

  //Checking form post url values
  socket.on('join', (params, callback) => {
    if (!isRealString(params.name) || !isRealString(params.room)) {
      return callback('Name and room name are required.');
    }

    socket.join(params.room);
    users.removeUser(socket.id);
    users.addUser(socket.id, params.name, params.room);

    io.to(params.room).emit('updateUserList', users.getUserList(params.room));
    socket.emit('newMessage', generateMessage('Stunkki (from server)', 'Welcome to Stunkki\'s super chat. You can interact with Sam Dog by entering message sam.'));
    socket.emit('newMessage', generateMessage('Stunkki (from server)', `Latest features:
    <br><ul><li>Full width video</li>
    <li>Rooms</li>
    <li>Usernames</li></ul>
    <p>Bug fixes:</p>
    <ul><li>Autoscroll</li></ul>`));

    // <li>Google search</li>
    // <li>Forecast</li></ul>

    socket.broadcast.to(params.room).emit('newMessage', generateMessage('Stunkki (from server)', `${params.name} has joined`));
    callback();
  });

  socket.on('createMessage', (message, callback) => {
    // console.log('createMessage', message); // DEBUG
    var user = users.getUser(socket.id);

    if (user && isRealString(message.text)) {
      io.to(user.room).emit('newMessage', generateMessage(user.name, message.text));

    }
    callback('message from space... i mean server');
  })

  // socket.broadcast.emit('newMessage', generateMessage(message.from, message.text));

  socket.on('createLocationMessage', (coords) => {
    console.log(coords);
    var user = users.getUser(socket.id );

    if (user) {
      io.emit('newLocationMessage', generateLocationMessage(user.name, coords.latitude, coords.longitude));
    }
  });

  socket.on('disconnect', () => {
    console.log('User was disconnected');
    const user = users.removeUser(socket.id);

    if (user) {
      io.to(user.room).emit('updateUserList', users.getUserList(user.room));
      io.to(user.room).emit('newMessage', generateMessage('Admin', `${user.name} has left`));
    }
  });
});

server.listen(port, () => {
  console.log(`Server is up on port ${port}`);
})
